class File:
    def __init__(self):
        self.items = []

    def enqueue(self, item):

        if self.is_empty(): # Si la liste est vide
            self.items.append(item) # Ajouter le tuple à la liste
        else: # Sinon
            for i in range(self.size()): # Pour chaque tuple de la liste
                if item[0] < self.items[i][0]: # Si l'ordre de priorité du tuple à ajouter est plus petit que le tuple de la liste
                    self.items.insert(i, item) # Ajouter le tuple à la liste juste avant celui avec un ordre de priorité plus grand
                    return

            self.items.append(item) # Si le tuple a un ordre de priorité plus grand que tous les tuples de la liste, il faut l'ajouter à la fin de la liste

    def dequeue(self):

        if not self.is_empty(): # Si la liste n'est pas vide
            return self.items.pop(0) # Enlever le premier tuple de la liste (il est prioritaire, car il a un ordre de priorité plus petit)

    def is_empty(self): # Pour vérifier si la liste est vide
        return (self.items == [])

    def size(self): # Pour avoir le nombre de tuple dans la liste
        return len(self.items)

    def afficher(self): # Pour afficher les tuples
        return self.items


if __name__ == "__main__": #Exemple pour tester les méthodes

    file_attente_prioritaire = File()
    file_attente_prioritaire.enqueue((4, "Nathan"))
    file_attente_prioritaire.enqueue((2, "Julia"))
    file_attente_prioritaire.enqueue((1, "Amandine"))
    file_attente_prioritaire.enqueue((3, "Mathias"))
    print("File d'attente actuelle : " + str(file_attente_prioritaire.afficher()))
    file_attente_prioritaire.dequeue()
    print("File d'attente actuelle : " + str(file_attente_prioritaire.afficher()))
    file_attente_prioritaire.dequeue()
    print("File d'attente actuelle : " + str(file_attente_prioritaire.afficher()))